// server.js
require('dotenv').config({path:__dirname + '/.env'});
const express = require('express')
const mysql = require('mysql')
const connection = mysql.createConnection({
  host: `${process.env.DB_HOST}`,
  user: `${process.env.DB_USER}`,
  password: `${process.env.DB_PASSWORD}`,
  database: `${process.env.DB_NAME}`
})
connection.connect();

const STATUS_NOT_PLAYED = 0;
const STATUS_SOLVED = 1;
const STATUS_INVALID = 2;

const app = express()
const port = process.env.PORT || 3000;

app.get('/', handleRequest)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

 async function createTiles(rows) {
  let tiles = await Promise.all(
    rows.map(async(val) => {
      let entries = await createOptions(val.qid,val.pais)
      entries.push({ "type": "white", "decision": "skip", "label": "Siguiente" });
      entries.push({ "type": "blue", "decision": "no", "label": "Ninguna opción es la correcta" });
      console.log(entries)
      let sections= [ {
        "type": "item",
        "q": val.qid
      }];
      if (val.website) {
        sections.push(
          {
            "type": "text",
            "title": "Sitio web oficial",
            "url": val.website,
            "text": val.website
          }
        )
      }
      return {
        id: val.qid,
        sections: sections,
        controls: [{
            "type": "buttons",
            "entries": entries
        }]
      };
    })
  );
  console.log(tiles)
  return {
    "tiles" : tiles
  }
}

async function createOptions(qid){
  let daysOpen = await getDaysOpen()
  console.log(daysOpen)
  let options = daysOpen.map(function (val, index) {
  console.log(val)
  let itemId = val.qid.replaceAll("Q","")
    return {
      "type": "green",
      "decision": "yes",
      "label": val.label,
      "api_action": {
          "action": "wbcreateclaim",
          "entity": qid,
          "property": "P3025",
          "snaktype": "value",
          "value": `{\"entity-type\":\"item\",\"numeric-id\":${itemId}}`
      }
    }
  });
  return options
}

async function getDaysOpen(country) {
  maxOptions = 10
  sqlQuery = `SELECT * 
    FROM dias
    ORDER BY sorting ASC
    LIMIT ${maxOptions};`

  let days = await queryDatabase(sqlQuery)
  console.log(days)
  return days
}

function queryDatabase(sqlQuery) {
  return new Promise((resolve, reject) => {
    connection.query(sqlQuery, (err, rows, fields) => {
      if (err) {
        console.log("FALLÓ LA QUERY")
        reject(err);
      } else {
        resolve(rows);
      }
    });
  });
}

function logNo(id, user) {
  console.log("log no")
  sqlQuery = `UPDATE tiles
  SET status = ${STATUS_INVALID}, usuario = '${user}'
  WHERE qid = '${id}';`;
  console.log(sqlQuery)
  queryDatabase(sqlQuery);
}

function logYes(id, user) {
  console.log("log yes")
  sqlQuery = `UPDATE tiles
  SET status = ${STATUS_SOLVED}, usuario = '${user}'
  WHERE qid = '${id}';`;
  console.log(sqlQuery)
  queryDatabase(sqlQuery);
}

async function handleRequest(req, res) {
  try {
    const action = req.query.action;
    const callback = req.query.callback;
    switch (action) {
      case "desc":
        let descJson = {
          "label" : {
            "en" : "Latin America in Wikidata Contest 2023 - GLAM",
            "es" : "Concurso Latinoamerica en Wikidata 2023 - Instituciones Patrimoniales",
            "pt" : "Concurso América Latina na Wikidata - Instituições Patrimoniais"
          },
          "description" : {
            "en" : "In the 2023 edition of the Latin America in Wikidata contest, we invite you to contribute to Wikidata by improving content related to cultural heritage in Latin America.</br>There are two game modes. In this case, you will have to complete the days in which the heritage institutions are open to the public.</br>If you know the correct answer but it does not appear among the options, you can touch the name of the institution to go to the element in Wikidata and edit this property manually. The manual editions in the account for the contest but are a great contribution.</br>The contest is from November 14, 2023 at 00:01 UTC on December 14, 2023 at 23:59 UTC, until the items are closed available. It arises from the collaboration between Wikimedia Argentina, Wikimedistas de Bolivia, Wikimedia Chile, Wikimedia Colombia, Wikimedia Mexico, Wikimedistas de Uruguay and Wikimedia Venezuela.",
            "es" : "En la edición 2023 del concurso Latinoamérica en Wikidata te invitamos a contribuir en Wikidata mejorando contenido vinculado al patrimonio cultural en América Latina.</br>Hay dos modalidades del juego. En esta, tendrás que completar los días en que las instituciones patrimoniales están abiertas al público.</br>Si sabes la respuesta correcta pero no aparece entre las opciones, puedes tocar el nombre de la institución para ir al elemento en Wikidata y editar esta propiedad manualmente. Las ediciones manuales no cuentan para el concurso pero son una gran contribución.</br>El concurso es desde el 14 de noviembre 2023 a las 00:01 UTC al 14 de diciembre 2023 a las 23:59 UTC, o hasta agotar los ítems disponibles. Surge de la colaboración entre Wikimedia Argentina, Wikimedistas de Bolivia, Wikimedia Chile, Wikimedia Colombia, Wikimedia Mexico, Wikimedistas de Uruguay y Wikimedia Venezuela.",
            "pt" : "Na edição 2023 do concurso Latinoamérica no Wikidata, você convida você a contribuir no Wikidata melhorando o conteúdo vinculado ao patrimônio cultural da América Latina.</br>Hay das modalidades do jogo. Neste caso, você terá que completar os dias em que as instituições patrimoniais estejam abertas ao público.</br>Se você souber a resposta correta, mas não aparecer entre as opções, você pode tocar o nome da instituição para ir para qualquer elemento no Wikidata e editar este propriedade manualmente. As edições manuais não contam para o concurso, mas são uma grande contribuição.</br>O concurso será de 14 de novembro de 2023 às 00:01 UTC até 14 de dezembro de 2023 às 23:59 UTC, ou até o final dos itens disponíveis. Aumento da colaboração entre Wikimedia Argentina, Wikimedistas da Bolívia, Wikimedia Chile, Wikimedia Colômbia, Wikimedia México, Wikimedistas do Uruguai e Wikimedia Venezuela.",
          },
          "icon" : 'https://upload.wikimedia.org/wikipedia/commons/6/66/GLAMWikiConfGalleryIcon.png'
        }
        res.send(`${callback}(${JSON.stringify(descJson)})`);
        break;
      
      case "tiles":
        console.log("requested tiles")
        const num = req.query?.num ? req.query.num : 5;
        
        sqlQuery = `SELECT * 
        FROM tiles
        WHERE status = ${STATUS_NOT_PLAYED}
        ORDER BY RAND()
        LIMIT ${num};`

        let institutions = await queryDatabase(sqlQuery)
        tiles = await createTiles(institutions);
        console.log("tiles")
        console.log(tiles)
        res.send(`${callback}(${JSON.stringify(tiles)})`);
        break;

      case "log_action":
        let decision = req.query.decision
        let user = req.query.user
        let tile = req.query.tile
        console.log("made a decision")
        console.log(decision)
        
        if (decision=="yes") {
          logYes(tile,user)
        } else if (decision=="no"){
          logNo(tile,user)
        }
        break;
    
      default:
        res.json({
          error: 'Parámetro "action" inválido'
        });
    }
  } catch (error) {
    console.error('Uncaught Exception:', error);
  }
}

process.on('uncaughtException', (error) => {
  console.error('Uncaught Exception:', error);
  // Realizar acciones de limpieza si es necesario
  process.exit(1); // Forzar la salida de la aplicación con un código de error
});